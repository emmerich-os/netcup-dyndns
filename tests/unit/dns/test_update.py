import pytest

from netcup_dyndns import models
from netcup_dyndns import testing
from netcup_dyndns.dns import update


@pytest.mark.parametrize(
    ("record", "hostname", "expected"),
    [
        (testing.FakeDNSRecord(hostname="test"), "test", True),
        (testing.FakeDNSRecord(hostname="test"), "test_not_matching", False),
    ],
)
def test_hostname_machting(record, hostname, expected):
    result = update._hostnames_matching(record, hostname=hostname)

    assert result == expected


@pytest.mark.parametrize(
    ("record", "destination", "expected"),
    [
        (
            testing.FakeDNSRecord(destination=testing.VALID_IPv4),
            models.IPv4(testing.VALID_IPv4_CHANGED),
            True,
        ),
        (
            testing.FakeDNSRecord(destination=testing.VALID_IPv4),
            models.IPv4(testing.VALID_IPv4),
            False,
        ),
        (
            testing.FakeDNSRecord(destination=testing.VALID_IPv6),
            models.IPv6(testing.VALID_IPv6_CHANGED),
            True,
        ),
        (
            testing.FakeDNSRecord(destination=testing.VALID_IPv6),
            models.IPv6(testing.VALID_IPv6),
            False,
        ),
    ],
)
def test_destination_has_changed(record, destination, expected):
    result = update._destination_has_changed(record, destination=destination)

    assert result == expected


@pytest.mark.parametrize(
    ("record", "destination", "expected"),
    [
        (
            testing.FakeDNSRecord(
                id=1, hostname="test_hostname", type="A", destination=testing.VALID_IPv4
            ),
            models.IPv4(testing.VALID_IPv4_CHANGED),
            testing.FakeDNSRecord(
                id=1,
                hostname="test_hostname",
                type="A",
                destination=testing.VALID_IPv4_CHANGED,
            ),
        ),
        (
            testing.FakeDNSRecord(
                id=1,
                hostname="test_hostname",
                type="AAAA",
                destination=testing.VALID_IPv6,
            ),
            models.IPv6(testing.VALID_IPv6_CHANGED),
            testing.FakeDNSRecord(
                id=1,
                hostname="test_hostname",
                type="AAAA",
                destination=testing.VALID_IPv6_CHANGED,
            ),
        ),
    ],
)
def test_create_updated_dns_record(record, destination, expected):
    result = update._create_updated_dns_record(record, destination=destination)

    assert result.id == expected.id
    assert result.hostname == expected.hostname
    assert result.type == expected.type
    assert result.destination == expected.destination
