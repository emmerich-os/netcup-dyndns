import pytest

from netcup_dyndns import models
from netcup_dyndns import testing
from netcup_dyndns.dns import create


@pytest.mark.parametrize(
    ("domain", "hostnames", "record_type", "destination"),
    [
        (
            "test_domain",
            ["test1", "test2"],
            models.DNSRecordTypeA(),
            models.IPv4(testing.VALID_IPv4),
        ),
        (
            "test_domain",
            ["test1", "test2"],
            models.DNSRecordTypeAAAA(),
            models.IPv6(testing.VALID_IPv6),
        ),
    ],
)
def test_create_dns_records(domain, hostnames, record_type, destination):
    client = testing.FakeClient()
    expected = [
        testing.FakeDNSRecord(
            hostname=hostname,
            type=record_type.name,
            destination=destination.address,
        )
        for hostname in hostnames
    ]

    create.create_dns_records(
        domain,
        hostnames=hostnames,
        record_type=record_type,
        destination=destination,
        client=client,
    )
    result = client.created[domain]

    for res, exp in zip(result, expected):
        assert res.hostname == exp.hostname
        assert res.type == exp.type
        assert res.destination == exp.destination
