import nc_dnsapi as netcup

from netcup_dyndns import models
from netcup_dyndns import testing
from netcup_dyndns.dns import connect


def test_create_connection(monkeypatch):
    credentials = models.NetcupAPICredentials(
        customer_id=1,
        api_key="test_api_key",
        api_password="test_api_password",
    )
    monkeypatch.setattr(
        netcup,
        "Client",
        testing.FakeClient,
    )

    result = connect.create_connection(credentials)

    assert isinstance(result, testing.FakeClient)
