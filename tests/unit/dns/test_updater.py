from unittest import mock

import pytest

from netcup_dyndns import models
from netcup_dyndns import testing
from netcup_dyndns.dns import updater


@pytest.fixture()
def credentials() -> models.NetcupAPICredentials:
    return models.NetcupAPICredentials(
        customer_id=1,
        api_key="test_api_key",
        api_password="test_api_password",
    )


@pytest.mark.parametrize(
    ("existing_records", "record_type", "destination", "expected"),
    [
        # Update type A records
        (
            [
                testing.FakeDNSRecord(
                    id=1,
                    hostname="test1",
                    type="A",
                    destination=testing.VALID_IPv4,
                ),
                testing.FakeDNSRecord(
                    id=2,
                    hostname="test2",
                    type="A",
                    destination=testing.VALID_IPv4,
                ),
                testing.FakeDNSRecord(
                    id=3,
                    hostname="test3",
                    type="AAAA",
                    destination=testing.VALID_IPv6,
                ),
            ],
            models.DNSRecordTypeA(),
            models.IPv4(testing.VALID_IPv4_CHANGED),
            [
                testing.FakeDNSRecord(
                    id=1,
                    hostname="test1",
                    type="A",
                    destination=testing.VALID_IPv4_CHANGED,
                ),
                testing.FakeDNSRecord(
                    id=2,
                    hostname="test2",
                    type="A",
                    destination=testing.VALID_IPv4_CHANGED,
                ),
            ],
        ),
        # Update type AAAA records
        (
            [
                testing.FakeDNSRecord(
                    id=1,
                    hostname="test1",
                    type="AAAA",
                    destination=testing.VALID_IPv6,
                ),
                testing.FakeDNSRecord(
                    id=2,
                    hostname="test2",
                    type="AAAA",
                    destination=testing.VALID_IPv6,
                ),
                testing.FakeDNSRecord(
                    id=3,
                    hostname="test3",
                    type="A",
                    destination=testing.VALID_IPv4,
                ),
            ],
            models.DNSRecordTypeAAAA(),
            models.IPv6(testing.VALID_IPv6_CHANGED),
            [
                testing.FakeDNSRecord(
                    id=1,
                    hostname="test1",
                    type="AAAA",
                    destination=testing.VALID_IPv6_CHANGED,
                ),
                testing.FakeDNSRecord(
                    id=2,
                    hostname="test2",
                    type="AAAA",
                    destination=testing.VALID_IPv6_CHANGED,
                ),
            ],
        ),
    ],
)
@testing.patch_backoff
def test_update_or_create_dns_records_destination_to_ip_address(
    monkeypatch, credentials, existing_records, record_type, destination, expected
):
    client = testing.FakeClient(contains=existing_records)

    monkeypatch.setattr(
        updater.connect,
        "create_connection",
        mock.MagicMock(return_value=client),
    )

    updater.update_or_create_dns_records_destination_to_ip_address(
        domain="test_domain",
        record_type=record_type,
        credentials=credentials,
        destination=destination,
        hostnames_to_update=["test1", "test2"],
    )
    result = client.updated

    for res, exp in zip(result, expected):
        assert res.id == exp.id
        assert res.hostname == exp.hostname
        assert res.type == exp.type
        assert res.destination == exp.destination


@pytest.mark.parametrize(
    ("records", "hostnames_to_update", "expected"),
    [
        (
            [
                testing.FakeDNSRecord(hostname="test1"),
                testing.FakeDNSRecord(hostname="test2"),
            ],
            ["test1", "test2"],
            ["test1", "test2"],
        ),
        ([testing.FakeDNSRecord(hostname="test1")], ["test1", "test2"], ["test1"]),
    ],
)
def test_get_existing_hostnames(records, hostnames_to_update, expected):
    result = updater._get_existing_hostnames(
        records, hostnames_to_update=hostnames_to_update
    )

    assert result == expected


@pytest.mark.parametrize(
    ("records", "hostnames_to_update", "expected"),
    [
        (
            [
                testing.FakeDNSRecord(hostname="test1"),
                testing.FakeDNSRecord(hostname="test2"),
            ],
            ["test1", "test2"],
            [],
        ),
        ([testing.FakeDNSRecord(hostname="test1")], ["test1", "test2"], ["test2"]),
    ],
)
def test_get_non_existing_hostnames(records, hostnames_to_update, expected):
    result = updater._get_non_existing_hostnames(
        records, hostnames_to_update=hostnames_to_update
    )

    assert result == expected
