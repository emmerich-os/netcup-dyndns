import pytest

from netcup_dyndns import testing
from netcup_dyndns.models.apis import fritzbox


@pytest.fixture()
def api() -> fritzbox.FritzBoxAPI:
    return fritzbox.FritzBoxAPI(
        address="test_fritzbox_address",
    )


class TestExternalAPI:
    @pytest.mark.parametrize(
        ("ip", "expected"),
        [
            (
                "",
                fritzbox.NoPublicIPv4AssignedException(),
            ),
            (
                testing.VALID_IPv4,
                testing.VALID_IPv4,
            ),
        ],
    )
    @testing.patch_backoff
    def test_get_ipv4(self, monkeypatch, api, ip, expected):
        testing.FakeFritzStatus.ipv4 = ip
        monkeypatch.setattr(
            fritzbox.fritzstatus,
            "FritzStatus",
            testing.FakeFritzStatus,
        )

        with testing.expect_raise_if_exception(expected):
            result = api._get_ipv4()

            assert result == expected

    @pytest.mark.parametrize(
        ("ip", "expected"),
        [
            (
                "",
                fritzbox.NoPublicIPv6AssignedException(),
            ),
            (
                testing.VALID_IPv6,
                testing.VALID_IPv6,
            ),
        ],
    )
    @testing.patch_backoff
    def test_get_ipv6(self, monkeypatch, api, ip, expected):
        testing.FakeFritzStatus.ipv6 = ip
        monkeypatch.setattr(
            fritzbox.fritzstatus,
            "FritzStatus",
            testing.FakeFritzStatus,
        )

        with testing.expect_raise_if_exception(expected):
            result = api._get_ipv6()

            assert result == expected
