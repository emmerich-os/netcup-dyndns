from unittest import mock

import pytest

from netcup_dyndns import testing
from netcup_dyndns.models.apis import external


@pytest.fixture()
def api() -> external.ExternalAPI:
    return external.ExternalAPI(
        ipv4="test_ipv4_address",
        ipv6="test_ipv6_address",
    )


class TestExternalAPI:
    @pytest.mark.parametrize(
        ("ip", "status_code", "expected"),
        [
            (
                "",
                404,
                external.InvalidResponseException(),
            ),
            (
                testing.VALID_IPv4,
                200,
                testing.VALID_IPv4,
            ),
        ],
    )
    @testing.patch_backoff
    def test_get_ipv4(self, monkeypatch, api, ip, status_code, expected):
        response = testing.FakeResponse(
            content=ip,
            status_code=status_code,
        )
        monkeypatch.setattr(
            external.requests,
            "get",
            mock.MagicMock(return_value=response),
        )

        with testing.expect_raise_if_exception(expected):
            result = api._get_ipv4()

            assert result == expected

    @pytest.mark.parametrize(
        ("ip", "status_code", "expected"),
        [
            (
                "",
                404,
                external.InvalidResponseException(),
            ),
            (
                testing.VALID_IPv6,
                200,
                testing.VALID_IPv6,
            ),
        ],
    )
    @testing.patch_backoff
    def test_get_ipv6(self, monkeypatch, api, ip, status_code, expected):
        response = testing.FakeResponse(
            content=ip,
            status_code=status_code,
        )
        monkeypatch.setattr(
            external.requests,
            "get",
            mock.MagicMock(return_value=response),
        )

        with testing.expect_raise_if_exception(expected):
            result = api._get_ipv6()

            assert result == expected
