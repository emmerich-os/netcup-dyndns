import pytest

from netcup_dyndns import models
from netcup_dyndns import testing
from netcup_dyndns.models.apis import abc


class FakeAPI(abc.AbstractAPI):
    def _get_ipv4(self):
        return testing.VALID_IPv4

    def _get_ipv6(self):
        return testing.VALID_IPv6


@pytest.fixture()
def fake_api():
    return FakeAPI()


@pytest.mark.parametrize(
    ("record_type", "expected"),
    [
        (
            models.AbstractDNSRecordType(),
            NotImplementedError(),
        ),
        (
            models.DNSRecordTypeA(),
            models.IPv4(testing.VALID_IPv4),
        ),
        (
            models.DNSRecordTypeAAAA(),
            models.IPv6(testing.VALID_IPv6),
        ),
    ],
)
@testing.patch_backoff
def test_get_public_ip(fake_api, record_type, expected):
    with testing.expect_raise_if_exception(expected):
        result = fake_api.get_public_ip(record_type)

        assert result == expected
