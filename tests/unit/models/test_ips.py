import pytest

from netcup_dyndns import testing
from netcup_dyndns.models import ips


class TestIPv4:
    @pytest.mark.parametrize(
        ("ip", "expected"),
        [
            (testing.INVALID_IPv4, ips.InvalidIpTypeException()),
            (testing.VALID_IPv6, ips.InvalidIpTypeException()),
            (testing.VALID_IPv4, testing.VALID_IPv4),
        ],
    )
    def test_init(self, ip, expected):
        with testing.expect_raise_if_exception(expected):
            result = ips.IPv4(ip)

            assert result.address == expected


class TestIPv6:
    @pytest.mark.parametrize(
        ("ip", "expected"),
        [
            (testing.INVALID_IPv6, ips.InvalidIpTypeException()),
            (testing.VALID_IPv4, ips.InvalidIpTypeException()),
            (testing.VALID_IPv6, testing.VALID_IPv6),
        ],
    )
    def test_init(self, ip, expected):
        with testing.expect_raise_if_exception(expected):
            result = ips.IPv6(ip)

            assert result.address == expected
