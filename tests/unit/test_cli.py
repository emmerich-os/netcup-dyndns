from unittest import mock

import click.testing
import pytest
import requests

import netcup_dyndns.dns.connect
from netcup_dyndns import cli
from netcup_dyndns import testing


@pytest.fixture(params=[False, True])
def use_fritzbox(request):
    return request.param


@pytest.mark.parametrize(
    ("ipv4", "ipv6", "expect_update"),
    [
        (
            testing.VALID_IPv4_CHANGED,
            testing.VALID_IPv6_CHANGED,
            True,
        ),
        (
            testing.INVALID_IPv4,
            testing.INVALID_IPv6,
            False,
        ),
        (
            "",
            "",
            False,
        ),
    ],
)
@testing.patch_backoff
def test_update_dns_records_ipv4(monkeypatch, ipv4, ipv6, expect_update):
    existing_records = [
        testing.FakeDNSRecord(
            id=1,
            hostname="test1",
            type="A",
            destination=testing.VALID_IPv4,
        ),
        testing.FakeDNSRecord(
            id=2,
            hostname="test2",
            type="AAAA",
            destination=testing.VALID_IPv6,
        ),
    ]
    expected_records = [
        testing.FakeDNSRecord(
            id=1,
            hostname="test1",
            type="A",
            destination=ipv4,
        ),
        testing.FakeDNSRecord(
            id=2,
            hostname="test2",
            type="AAAA",
            destination=ipv6,
        ),
    ]
    client = testing.FakeClient(contains=existing_records)
    responses = [
        testing.FakeResponse(status_code=200, content=ipv4),
        testing.FakeResponse(status_code=200, content=ipv6),
    ]

    monkeypatch.setattr(
        netcup_dyndns.dns.connect,
        "create_connection",
        mock.MagicMock(return_value=client),
    )

    # Note: Dangerous implementation of responses since it depends on the
    # order of the calls in the module
    iter_responses = iter(responses)
    monkeypatch.setattr(
        requests,
        "get",
        lambda *args, **kwargs: next(iter_responses),
    )

    testing.FakeFritzStatus.ipv4 = ipv4
    testing.FakeFritzStatus.ipv6 = ipv6
    monkeypatch.setattr(
        netcup_dyndns.models.apis.fritzbox.fritzstatus,
        "FritzStatus",
        testing.FakeFritzStatus,
    )

    runner = click.testing.CliRunner()

    cli_args = [
        "--domain",
        "test_domain",
        "--hostnames",
        "test1,test2",
        "--customer-id",
        "1",
        "--api-key",
        "test_api_key",
        "--api-password",
        "test_api_password",
    ]
    if use_fritzbox:
        cli_args.extend(["--fritzbox-address", "test_fritzbox_address"])

    result = runner.invoke(
        cli.update_dns_records,
        cli_args,
    )
    result_records = client.updated

    assert result.exit_code == 0

    if expect_update:
        for res, exp in zip(result_records, expected_records):
            assert res.id == exp.id
            assert res.hostname == exp.hostname
            assert res.type == exp.type
            assert res.destination == exp.destination
    else:
        assert not result_records
