import functools

import pytest

from netcup_dyndns import utils


class TestException1(Exception):
    pass


class TestException2(Exception):
    pass


@pytest.mark.parametrize(
    ("string", "expected"),
    [
        ("test1", ["test1"]),
        ("test1,", ["test1"]),
        ("test1,test2", ["test1", "test2"]),
    ],
)
def test_split_comma_separated_string(string, expected):
    result = utils.split_comma_separated_string(string)

    assert result == expected


@pytest.mark.parametrize(
    ("description", "exceptions", "function", "args", "kwargs", "expected"),
    [
        (
            "test description",
            TestException1,
            lambda x: x,
            ["test"],
            {},
            "test",
        ),
    ],
)
def test_call_with_backoff(
    monkeypatch, description, exceptions, function, args, kwargs, expected
):
    called_with = []

    def mock_decorator(*decorator_args, **decorator_kwargs):
        if decorator_args:
            called_with.append(decorator_args)
        if decorator_kwargs:
            called_with.append(decorator_kwargs)

        def decorator(target):
            @functools.wraps(target)
            def wrapper(*target_args, **target_kwars):
                return target(*target_args, **target_kwars)

            return wrapper

        return decorator

    monkeypatch.setattr(
        utils.backoff,
        "on_exception",
        mock_decorator,
    )

    result = utils.call_with_backoff(
        task_description=description,
        exceptions=exceptions,
    )(function)(*args, **kwargs)

    if args or kwargs:
        assert called_with
    assert result == expected
