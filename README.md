# Netcup DynDNS
[![pipeline status](https://gitlab.com/emmerich-os/netcup-dyndns/badges/main/pipeline.svg)](https://gitlab.com/emmerich-os/netcup-dyndns/-/commits/main)
[![coverage report](https://gitlab.com/emmerich-os/netcup-dyndns/badges/main/coverage.svg?job=coverage)](https://gitlab.com/emmerich-os/netcup-dyndns/-/commits/main)
[![](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Dedicated DynDNS package for the netcup API using the [nc-dnsapi](https://pypi.org/project/nc-dnsapi/) package.

Retrieves the public IPv4 and IPv6 from an API (external or internal (FritzBox)) and updates a set of DNS records (hostnames) for a given domain using the netcup API.

## Usage

Can be simply used from the CLI as
```commandline
$ netcup-dyndns update
```
or by running it in Docker.

The `update` command, however, uses arguments (read environment variables if not explicitly passed) to gain access to the netcup API and to know from which source to retrieve the IPs.

The (partly required) enviroment variables are as follows:

- `DOMAIN`: domain whose DNS records to update (`example.com`)
- `HOSTNAMES`: comma-separated list of hostnames to update (`*,subdomain1,subdomain2`)
- `NETCUP_CUSTOMER_ID`: customer ID at netcup
- `NETCUP_API_KEY`: key to the netcup API
- `NETCUP_API_PASSWORD`: password to the API
- `FRITZBOX_ADDRESS` (optional): address of the fritzbox in the LAN (if not given, IPs will be retrieved from [`ipify`](https://www.ipify.org/))
