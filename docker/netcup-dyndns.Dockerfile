# Building layer
FROM fabianemmi/python-poetry:3.9-1.1.7-slim as builder

# Copy files
ADD netcup_dyndns /app/netcup_dyndns
COPY poetry.lock pyproject.toml /app/
WORKDIR /app

# Install with poetry
RUN python -m venv /venv
RUN /venv/bin/python -m pip install --upgrade pip
RUN . /venv/bin/activate && poetry install --no-dev --no-root --no-interaction --no-ansi
RUN . /venv/bin/activate && poetry build
RUN . /venv/bin/activate && pip install /app/dist/*.whl

# Delete Python cache files
RUN find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
RUN cd /venv && find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf


# Execution layer
FROM python:3.9-alpine

COPY --from=builder /venv /venv

# Create entrypoint script
ENTRYPOINT . /venv/bin/activate &&  netcup-dyndns update
