import functools
import logging
from typing import Callable
from typing import Union

import backoff  # type: ignore

from . import config

logger = logging.getLogger(__name__)


def split_comma_separated_string(string: str) -> list[str]:
    """Split a string with comma-separated values into a list."""
    split = string.split(",")
    split_cleared = [element for element in split if element]
    logger.info("Input %s was split to %s", string, split_cleared)
    return split_cleared


def call_with_backoff(
    task_description: str,
    exceptions: Union[type[Exception], tuple[type[Exception]]],
) -> Callable:
    """Call with backoff."""

    def decorator(target: Callable) -> Callable:
        @functools.wraps(target)
        def wrapper(*args, **kwargs) -> Callable:
            return backoff.on_exception(
                **config.DEFAULT_BACKOFF_PARAMS,
                exception=exceptions,
                on_backoff=lambda details: logger.info(
                    "Backing off from attempting to: %s", task_description, details
                ),
                on_giveup=lambda details: logger.info(
                    "Giving up from attempting to %s: %s", task_description, details
                ),
            )(target)(*args, **kwargs)

        return wrapper

    return decorator
