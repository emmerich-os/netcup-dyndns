from typing import Optional

import click  # type: ignore

from . import api
from . import models
from . import updater
from . import utils
from .logger import setup_logger

logger = setup_logger()


@click.command(name="update")
@click.option(
    "domain",
    "--domain",
    "-d",
    envvar="DOMAIN",
    type=str,
    help="domain whose DNS records to update",
)
@click.option(
    "hostnames",
    "--hostnames",
    "-h",
    envvar="HOSTNAMES",
    type=str,
    help="comma-separated list of hostnames of type A whose destination to update",
)
@click.option(
    "customer_id",
    "--customer-id",
    "-c",
    envvar="NETCUP_CUSTOMER_ID",
    type=int,
    help="netcup customer ID",
)
@click.option(
    "api_key",
    "--api-key",
    envvar="NETCUP_API_KEY",
    type=str,
    help="netcup API key",
)
@click.option(
    "api_password",
    "--api-password",
    envvar="NETCUP_API_PASSWORD",
    type=str,
    help="netcup API password",
)
@click.option(
    "fritzbox_address",
    "--fritzbox-address",
    envvar="FRITZBOX_ADDRESS",
    type=str,
    help="address of the FritzBox in the LAN",
    default=None,
)
def update_dns_records(
    domain: str,
    hostnames: str,
    customer_id: int,
    api_key: str,
    api_password: str,
    fritzbox_address: Optional[str],
) -> None:
    """Update DNS records for given domain and hostnames to current IP of the system."""
    logger.info("Attempting to update DNS records %s for %s", hostnames, domain)
    hostnames_as_list = utils.split_comma_separated_string(hostnames)
    netcup_credentials = models.NetcupAPICredentials(
        customer_id=customer_id,
        api_key=api_key,
        api_password=api_password,
    )
    ip_api = api.get_api(fritzbox_address)
    for record_type in models.DNS_RECORD_TYPES:
        updater.update_or_create_dns_records(
            domain,
            record_type=record_type(),
            hostnames=hostnames_as_list,
            credentials=netcup_credentials,
            api=ip_api,
        )
