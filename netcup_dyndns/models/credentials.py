from dataclasses import dataclass


@dataclass
class NetcupAPICredentials:
    """The credentials for the netcup API."""

    customer_id: int
    api_key: str
    api_password: str
