from dataclasses import dataclass

from fritzconnection.lib import fritzstatus  # type: ignore

from .abc import AbstractAPI


class NoPublicIPv4AssignedException(Exception):
    """The internet connection has no public IPv4."""


class NoPublicIPv6AssignedException(Exception):
    """The internet connection has no public IPv6."""


@dataclass
class FritzBoxAPI(AbstractAPI):
    """FritzBox API to fetch public IPs from."""

    address: str = "fritz.box"

    @property
    def __connection(self) -> fritzstatus.FritzStatus:
        return fritzstatus.FritzStatus(address=self.address)

    def _get_ipv4(self) -> str:
        """Get public IPv4."""
        ip = self.__connection.external_ip
        if not ip:
            raise NoPublicIPv4AssignedException(
                "The FritzBox has no public IPv4 assigned"
            )
        return ip

    def _get_ipv6(self) -> str:
        """Get public IPv6."""
        ip = self.__connection.external_ipv6
        if not ip:
            raise NoPublicIPv6AssignedException(
                "The FritzBox has no public IPv6 assigned"
            )
        return ip
