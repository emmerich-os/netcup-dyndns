import functools
import logging
from dataclasses import dataclass
from typing import Callable

import requests  # type: ignore

from .abc import AbstractAPI
from netcup_dyndns import config
from netcup_dyndns import utils

logger = logging.getLogger(__name__)


class InvalidResponseException(Exception):
    """The response by the API is not 200."""


@dataclass
class ExternalAPI(AbstractAPI):
    """External API to fetch public IPs from."""

    ipv4: str = config.API_URL_IPV4
    ipv6: str = config.API_URL_IPV6

    def _get_ipv4(self) -> str:
        """Get public IPv4."""
        return _get_api_response(self.ipv4)

    def _get_ipv6(self) -> str:
        """Get public IPv6."""
        return _get_api_response(self.ipv6)


def call_with_backoff(func: Callable) -> Callable:
    """Call with backoff."""

    @functools.wraps(func)
    def wrapper(*args, **kwargs) -> Callable:
        return utils.call_with_backoff(
            task_description="get public IP from API",
            exceptions=InvalidResponseException,
        )(func)(*args, **kwargs)

    return wrapper


@call_with_backoff
def _get_api_response(address: str) -> str:
    response = requests.get(address)
    if not _request_successful(response):
        raise InvalidResponseException(
            f"API {address} returned invalid response {response.status_code}: {response.reason}"
        )
    content = response.text
    logger.info("Received response %s from API %s", content, address)
    return content


def _request_successful(response) -> bool:
    return response.status_code == 200
