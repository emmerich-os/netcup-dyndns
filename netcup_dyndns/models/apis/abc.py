import abc
import functools
import logging

from netcup_dyndns.models import ips
from netcup_dyndns.models import record_types


logger = logging.getLogger(__name__)


class AbstractAPI(abc.ABC):
    """Any API to fetch public IPs v4 and v6 from."""

    @functools.singledispatchmethod
    def get_public_ip(
        self, record_type: record_types.AbstractDNSRecordType
    ) -> ips.AbstractIP:
        """Get the public IP for a given DNS record type."""
        raise NotImplementedError(f"Unsupported record type {type(record_type)}")

    @get_public_ip.register
    def _get_public_ipv4(self, record_type: record_types.DNSRecordTypeA) -> ips.IPv4:
        """Get public IPv4."""
        logger.info("Requesting public IPv4 from %s", self)
        response = self._get_ipv4()
        logger.info("Current public IPv4 is %s", response)
        return ips.IPv4(response)

    @get_public_ip.register
    def _get_public_ipv6(self, record_type: record_types.DNSRecordTypeAAAA) -> ips.IPv6:
        """Get public IPv6."""
        logger.info("Requesting public IPv6 from %s", self)
        response = self._get_ipv6()
        logger.info("Current public IPv6 is %s", response)
        return ips.IPv6(response)

    @abc.abstractmethod
    def _get_ipv4(self) -> str:
        """Get public IPv4 from API."""

    @abc.abstractmethod
    def _get_ipv6(self) -> str:
        """Get public IPv6 from API."""
