import abc
import functools
import logging
import socket
from dataclasses import dataclass


logger = logging.getLogger(__name__)


class InvalidIpTypeException(Exception):
    """Received IP is not expected type (IPv4/6)."""


@dataclass
class AbstractIP(abc.ABC):
    """Any IP."""

    address: str

    def __post_init__(self):
        """Run validation of the IP address according to its type."""
        self.__validate_ip()

    def __validate_ip(self):
        """Validate the given address."""
        if not is_valid_ip(self):
            raise InvalidIpTypeException(
                "IP %s is not a valid %s", self.address, type(self)
            )


@dataclass
class IPv4(AbstractIP):
    """An IPv4."""


@dataclass
class IPv6(AbstractIP):
    """An IPv6."""


@functools.singledispatch
def is_valid_ip(ip: AbstractIP) -> bool:
    """Check whether given IP is a valid IP."""
    raise NotImplementedError(f"Unsupported type {type(ip)}")


@is_valid_ip.register
def _check_if_ipv4(ip: IPv4) -> bool:
    """Check if given IP is an IPv4."""
    logger.info("Checking if %s is a valid IPv4", ip)
    return _check_if_ip_type(ip, socket.AF_INET)


@is_valid_ip.register
def _check_if_ipv6(ip: IPv6) -> bool:
    """Check if given IP is an IPv6."""
    logger.info("Checking if %s is a valid IPv6", ip)
    return _check_if_ip_type(ip, socket.AF_INET6)


def _check_if_ip_type(ip: AbstractIP, ip_type: socket.AddressFamily) -> bool:
    try:
        socket.inet_pton(ip_type, ip.address)
    except OSError:
        return False
    else:
        return True
