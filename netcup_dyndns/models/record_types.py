import abc


class AbstractDNSRecordType(abc.ABC):
    """Any DNS record type."""

    name: str


class DNSRecordTypeA(AbstractDNSRecordType):
    """A DNS record of type A (IPv4)."""

    name = "A"


class DNSRecordTypeAAAA(AbstractDNSRecordType):
    """A DNS record of type AAAA (IPv6)."""

    name = "AAAA"


DNS_RECORD_TYPES: list[type[AbstractDNSRecordType]] = [
    DNSRecordTypeA,
    DNSRecordTypeAAAA,
]
