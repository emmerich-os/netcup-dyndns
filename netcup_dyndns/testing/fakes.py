from dataclasses import dataclass
from typing import Optional

from netcup_dyndns import models


class Fake:
    def __str__(self):
        return f"{self.__class__.__name__}({str(self.__dict__)})"

    def __repr__(self):
        return self.__str__()


@dataclass
class FakeResponse(Fake):
    status_code: int = 200
    reason: str = "test_reason"
    content: str = "test_content"

    @property
    def text(self) -> str:
        return self.content


@dataclass
class FakeDNSRecord(Fake):
    id: int = 1
    hostname: str = "test_hostname"
    type: str = "A"
    destination: str = "test_ip"


class FakeClient(Fake):
    contains: Optional[list[FakeDNSRecord]] = None

    def __init__(
        self,
        customer: int = 1,
        api_key: str = "test_api_key",
        api_password: str = "test_api_password",
        contains: Optional[list[FakeDNSRecord]] = None,
    ):
        self.customer = customer
        self.api_key = api_key
        self.api_password = api_password
        self.contains = contains or []
        self.updated: list[FakeDNSRecord] = []
        self.created: dict[str, list[FakeDNSRecord]] = {}

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return NotImplementedError

    def dns_records(self, domain: str) -> list[FakeDNSRecord]:
        return self.contains or []

    def update_dns_record(self, domain: str, record: FakeDNSRecord) -> None:
        rec = [rec for rec in self.contains if rec.id == record.id]  # type: ignore
        if len(rec) > 1:
            raise Exception("More than one record with ID %s in FakeClient", record.id)
        if rec:
            self.updated.append(record)

    def add_dns_record(self, domain: str, record: FakeDNSRecord) -> None:
        if domain not in self.created:
            self.created[domain] = []
        self.created[domain].append(record)


class FakeFritzStatus(Fake):
    ipv4: str = ""
    ipv6: str = ""

    def __init__(self, address: str = "test_fritzbox_address"):
        self.address = address

    @property
    def external_ip(self) -> str:
        return self.ipv4

    @property
    def external_ipv6(self) -> str:
        return self.ipv6


@dataclass
class FakeUnimplementedAPI(Fake, models.AbstractAPI):
    def _get_ipv4(self) -> str:
        return ""

    def _get_ipv6(self) -> str:
        return ""


@dataclass
class FakeExternalAPI(Fake, models.ExternalAPI):
    ipv4: str = ""
    ipv6: str = ""

    def _get_ipv4(self) -> str:
        return self.ipv4

    def _get_ipv6(self) -> str:
        return self.ipv6


@dataclass
class FakeFritzBoxAPI(Fake, models.FritzBoxAPI):
    ipv4: str = ""
    ipv6: str = ""

    def _get_ipv4(self) -> str:
        return self.ipv4

    def _get_ipv6(self) -> str:
        return self.ipv6
