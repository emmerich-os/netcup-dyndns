import functools
from typing import Callable
from unittest import mock


def patch_backoff(func: Callable) -> Callable:
    """Patch backoff parameters to avoid delay."""

    @functools.wraps(func)
    def wrapper(*args, **kwargs) -> Callable:
        return mock.patch("netcup_dyndns.config.DEFAULT_BACKOFF_PARAMS", {})(
            func(*args, **kwargs)
        )

    return wrapper
