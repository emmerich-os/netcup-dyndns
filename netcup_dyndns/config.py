import backoff  # type: ignore

API_URL_IPV4 = "https://api.ipify.org"
API_URL_IPV6 = "https://api64.ipify.org"
DEFAULT_BACKOFF_PARAMS = {
    "wait_gen": backoff.expo,
    "max_tries": 3,
    "max_time": 200,
}
