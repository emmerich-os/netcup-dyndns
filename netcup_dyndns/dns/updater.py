import logging

import nc_dnsapi as netcup  # type: ignore

from . import connect
from . import create
from . import fetch
from . import update
from netcup_dyndns import models

logger = logging.getLogger(__name__)


def update_or_create_dns_records_destination_to_ip_address(
    domain: str,
    record_type: models.AbstractDNSRecordType,
    credentials: models.NetcupAPICredentials,
    destination: models.AbstractIP,
    hostnames_to_update: list[str],
) -> None:
    """Update a set of DNS records for the given domain to point at a certain IP address."""
    logger.info(
        (
            "Updating or creating DNS records of type %s with "
            "hostnames %s for domain %s to point at %s"
        ),
        record_type,
        hostnames_to_update,
        domain,
        destination,
    )
    with connect.create_connection(credentials) as client:
        dns_records = fetch.get_dns_records(
            domain, record_type=record_type, client=client
        )
        existing_hostnames = _get_existing_hostnames(
            dns_records, hostnames_to_update=hostnames_to_update
        )
        non_existing_hostnames = _get_non_existing_hostnames(
            dns_records, hostnames_to_update=hostnames_to_update
        )
        update.update_dns_records(
            domain,
            records=dns_records,
            hostnames=existing_hostnames,
            destination=destination,
            client=client,
        )
        create.create_dns_records(
            domain,
            hostnames=non_existing_hostnames,
            record_type=record_type,
            destination=destination,
            client=client,
        )


def _get_existing_hostnames(
    records: list[netcup.DNSRecord], hostnames_to_update: list[str]
) -> list[str]:
    hostnames = _get_hostnames(records)
    existing_hostnames = hostnames & set(hostnames_to_update)
    logger.info("Existing hostnames are %s", existing_hostnames)
    return sorted(existing_hostnames)


def _get_non_existing_hostnames(
    records: list[netcup.DNSRecord], hostnames_to_update: list[str]
) -> list[str]:
    hostnames = _get_hostnames(records)
    non_existing_hostnames = set(hostnames_to_update) - hostnames
    logger.info("Non-existing hostnames are %s", non_existing_hostnames)
    return sorted(non_existing_hostnames)


def _get_hostnames(records: list[netcup.DNSRecord]) -> set[str]:
    hostnames = {record.hostname for record in records}
    logger.info("Hostnames from records %s are %s", records, hostnames)
    return hostnames
