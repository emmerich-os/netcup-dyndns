import itertools
import logging

import nc_dnsapi as netcup  # type: ignore

from netcup_dyndns import models
from netcup_dyndns import utils

logger = logging.getLogger(__name__)


def update_dns_records(
    domain: str,
    records: list[netcup.DNSRecord],
    hostnames: list[str],
    destination: models.AbstractIP,
    client: netcup.Client,
) -> None:
    """Update the DNS records for a given domain to point at a certain destination."""
    for record, hostname in itertools.product(records, hostnames):
        if _hostnames_matching(record, hostname) and _destination_has_changed(
            record, destination
        ):
            _update_record_destination(
                domain, client=client, record=record, destination=destination
            )


def _hostnames_matching(record: netcup.DNSRecord, hostname: str) -> bool:
    if record.hostname != hostname:
        return False
    logger.info("Hostname %s matches record's hostname: %s", hostname, record.__dict__)
    return True


def _destination_has_changed(
    record: netcup.DNSRecord, destination: models.AbstractIP
) -> bool:
    if record.destination == destination.address:
        return False
    logger.info(
        "Destination of DNS record %s is different to system's IP (%s)",
        record.__dict__,
        destination,
    )
    return True


@utils.call_with_backoff(
    task_description="update DNS records",
    exceptions=Exception,
)
def _update_record_destination(
    domain: str,
    client: netcup.Client,
    record: netcup.DNSRecord,
    destination: models.AbstractIP,
) -> None:
    updated_record = _create_updated_dns_record(record, destination=destination)
    logger.info(
        "Updating DNS record %s of domain %s to %s",
        record.__dict__,
        domain,
        updated_record.__dict__,
    )
    client.update_dns_record(domain, updated_record)


def _create_updated_dns_record(
    record: netcup.DNSRecord,
    destination: models.AbstractIP,
) -> netcup.DNSRecord:
    updated_record = netcup.DNSRecord(
        hostname=record.hostname,
        type=record.type,
        destination=destination.address,
        id=int(record.id),
    )
    logger.info("Created updated DNS record %s", updated_record.__dict__)
    return updated_record
