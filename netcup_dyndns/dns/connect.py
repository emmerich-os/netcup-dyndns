import logging

import nc_dnsapi as netcup  # type: ignore

from netcup_dyndns import models
from netcup_dyndns import utils

logger = logging.getLogger(__name__)


@utils.call_with_backoff(
    task_description="connect to netcup API",
    exceptions=Exception,
)
def create_connection(credentials: models.NetcupAPICredentials) -> netcup.Client:
    """Create a connection to the netcup API."""
    logging.info("Attempting to connect to netcup API")
    client = netcup.Client(
        customer=credentials.customer_id,
        api_key=credentials.api_key,
        api_password=credentials.api_password,
    )
    return client
