import logging

import nc_dnsapi as netcup  # type: ignore

from netcup_dyndns import models

logger = logging.getLogger(__name__)


def create_dns_records(
    domain: str,
    hostnames: list[str],
    record_type: models.AbstractDNSRecordType,
    destination: models.AbstractIP,
    client: netcup.Client,
) -> None:
    """Create a new DNS record for given type to point at a certain destination."""
    logger.info(
        "Creating DNS record for hostnames %s for domain %s to point at %s",
        hostnames,
        domain,
        destination,
    )
    for hostname in hostnames:
        record = _create_dns_record(
            hostname=hostname, record_type=record_type, destination=destination
        )
        _add_dns_record(domain, record=record, client=client)


def _create_dns_record(
    hostname: str,
    record_type: models.AbstractDNSRecordType,
    destination: models.AbstractIP,
) -> netcup.Client:
    record = netcup.DNSRecord(
        hostname=hostname, type=record_type.name, destination=destination.address
    )
    logger.info("Created DNS record %s", record.__dict__)
    return record


def _add_dns_record(
    domain: str, record: netcup.DNSRecord, client: netcup.Client
) -> None:
    logger.info(
        "Adding DNS record %s to domain %s",
        record.__dict__,
        domain,
    )
    client.add_dns_record(domain, record)
