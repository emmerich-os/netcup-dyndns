import logging

import nc_dnsapi as netcup  # type: ignore

from netcup_dyndns import models
from netcup_dyndns import utils

logger = logging.getLogger(__name__)


@utils.call_with_backoff(
    task_description="fetch DNS records",
    exceptions=Exception,
)
def get_dns_records(
    domain: str, record_type: models.AbstractDNSRecordType, client: netcup.Client
) -> list[netcup.DNSRecord]:
    """Get all hostnames for a certain domain and record type."""
    records = client.dns_records(domain)
    logger.info(
        "Fetched DNS records for domain %s: %s",
        domain,
        [rec.__dict__ for rec in records],
    )
    filtered_records = _filter_dns_records_by_type(records, by=record_type)
    return filtered_records


def _filter_dns_records_by_type(
    records: netcup.DNSRecord, by: models.AbstractDNSRecordType
) -> list[netcup.DNSRecord]:
    filtered_records = [record for record in records if record.type == by.name]
    logger.info("Filtered DNS records of type %s: %s", by, filtered_records)
    return filtered_records
