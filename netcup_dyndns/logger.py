import logging


def setup_logger(
    level: int = logging.INFO, name: str = "netcup-dyndns"
) -> logging.Logger:
    """Set up the logger.

    Parameters
    ----------
    level : int, default logging.INFO
        Logging level.
    name : str, optional
        Name of the logger.

    Returns
    -------
    logging.Logger

    """
    strfmt = (
        "%(levelname)s - %(asctime)s.%(msecs)03d - %(name)s:%(funcName)s:%(lineno)s\n"
        "\t%(message)s"
    )
    datefmt = "%Y/%m/%d %H:%M:%S"
    logging.basicConfig(
        level=level,
        format=strfmt,
        datefmt=datefmt,
    )
    logger = logging.getLogger(name)
    return logger
