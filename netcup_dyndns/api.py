import functools

from . import models


@functools.singledispatch
def get_api(address) -> models.AbstractAPI:
    """Get API to fetch IPs from."""
    return models.ExternalAPI()


@get_api.register
def _get_fritzbox_api(address: str) -> models.FritzBoxAPI:
    return models.FritzBoxAPI(address=address)
