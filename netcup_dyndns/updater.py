from . import dns
from . import models
from .logger import setup_logger

logger = setup_logger()


def update_or_create_dns_records(
    domain: str,
    hostnames: list[str],
    record_type: models.AbstractDNSRecordType,
    credentials: models.NetcupAPICredentials,
    api: models.AbstractAPI,
) -> None:
    """Update or create a DNS record."""
    logger.info("Attempting to update records of type %s", record_type)
    try:
        destination = api.get_public_ip(record_type)
    except (
        models.InvalidIpTypeException,
        models.InvalidResponseException,
        models.NoPublicIPv4AssignedException,
        models.NoPublicIPv6AssignedException,
    ):
        logger.error(
            "Cannot update hostname destinations for record type %s",
            record_type,
            exc_info=True,
        )
    else:
        dns.update_or_create_dns_records_destination_to_ip_address(
            domain,
            record_type=record_type,
            credentials=credentials,
            destination=destination,
            hostnames_to_update=hostnames,
        )
